import * as path from "path";
import * as fs from "fs";
import * as os from "os";
import * as vscode from "vscode";
import { ViewManager } from "./common/viewManager";
import { WebviewPanel, ExtensionContext } from "vscode";

export class BaseManager {
  extensionContext: ExtensionContext;

  constructor(extensionContext: ExtensionContext) {
    this.extensionContext = extensionContext;
  }
  
}
