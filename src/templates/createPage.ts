import * as path from "path";
import * as fs from "fs";
import * as os from "os";
import * as fse from 'fs-extra';
import * as vscode from "vscode";
import { WebviewPanel } from "vscode";
//import { gContext, createProject } from "../extension";
import * as utils from "../utils";
import { ViewManager } from "../common/viewManager";
import { BaseManager } from "../BaseManager";
import { CManager } from "./cManager";

export class CreatePage extends BaseManager {

    context: vscode.ExtensionContext;
    cObject: any;
    constructor(context: vscode.ExtensionContext) {
        super(context);
        this.context = context;
        this.cObject = new CManager(this.context);
    }

    /**
    * 定义项目创建时接收信息处理函数
    * @param viewPanel 页面
    * @param message 信息
    * @returns 
    */
    onReceiveListener = async (
        viewPanel: WebviewPanel,
        message: any
    ) => {
        //test
        console.log("CreatePage message " + message.type);
        //用户完成配置项目

        console.log("message " + message.type);
        if (message.type === "GET_HOME_DIR") {
            // 获取home路径并发送消息
            utils.onGetHomePath(this.extensionContext, viewPanel);
        } else if (message.type === "SET_DIR") {
            // 保存当前目录为默认目录
            console.log("SET_DIR: ");
            console.log(message.data);
            utils.onSetDir(
                this.extensionContext,
                message.data.isSaveDir,
                message.data.projectPath
            );
        } else if (message.type === "CHOOSE_DIR") {
            console.log("CHOOSE_DIR: ");
            console.log(message.data);
            // 选择目录，并发送消息
            utils.onChoosePrjDir(this.extensionContext, viewPanel);
        }
        else if (message.data.projectType === "c") {
            this.cObject.onReceiveListener(viewPanel, message);
        } else {
            utils.showError(message);
        }
        return;
    };


    /**
     * 注册创建默认模版命令
     * @returns 
     */
    registerCreatePage(): vscode.Disposable {
        // 注册命令
        let disposable = vscode.commands.registerCommand(
            "project.createProject",
            () => {
                // 打开webview窗口
                ViewManager.createWebviewPanel({
                    // html 文件名
                    path: "createProjectPage",
                    // 页面标题
                    title: "Create Project",
                    splitView: false,
                    // 监听
                    receiveListener: this.onReceiveListener,
                });
            }
        );
        return disposable;
    }
}