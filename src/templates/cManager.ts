import * as path from "path";
import * as fs from "fs";
import * as os from "os";
import * as fse from 'fs-extra';
import * as vscode from "vscode";
import { WebviewPanel } from "vscode";
//import { gContext, createProject } from "../extension";
import { gContext, modifyAuthorInfo, modifyCompilePath, modifyCMakeListsInfo } from "../extension";
import localize from "../localize";
import * as child_process from "child_process";
import { LangProConfig } from "../common/packageConfig";
import * as utils from "../utils";
import { ViewManager } from "../common/viewManager";
import { BaseManager } from "../BaseManager";
import { debug } from "console";

export class CManager extends BaseManager {
    /**
    * 定义项目创建时接收信息处理函数
    * @param viewPanel 页面
    * @param message 信息
    * @returns 
    */
    onReceiveListener = async (
        viewPanel: WebviewPanel,
        message: any
    ) => {
        //test
        console.log("CManager message " + message.type);
        //用户完成配置项目
        if (message.type === 'CHECK_DATA') {
            var msg = '';
            console.log("message.page: " + message.page);
            console.log("message.dataPages: ")
            console.log(message.dataPages);

            msg = await this.checkData(message, false);
            // //判断 qmake 路径
            // if (message.data.Build == 'qmake' && !fs.existsSync(message.data.Qmake)) {
            // 	msg = localize("language.errorQmakePath");
            // }
            // //判断 qmake 路径
            /*	 if (message.data.Build == 'qmake' && !fs.existsSync(message.data.Qmake)) {
                    msg = localize("language.errorQmakePath");
                    }*/

            // //判断 cmake 路径
            // if (message.data.Build == 'cmake' && !fs.existsSync(message.data.Cmake)) {
            // 	msg = localize("language.errorCmakePath");
            // }

            // //判断调试器路径
            // if (!fs.existsSync(message.data.Debugger)) {
            // 	msg = localize("language.errorDebuggerPath");
            // }

            // //判断编译器 C 路径
            // if (!fs.existsSync(message.data.CompileC)) {
            // 	msg = localize("language.errorCompileC");
            // }

            // //判断编译器 C++ 路径
            // if (!fs.existsSync(message.data.CompileCXX)) {
            // 	msg = localize("language.errorCompileCXX");
            // }
            var err = 'false';
            if (msg != '') {
                err = 'true'
            }
            // 发送消息
            viewPanel.webview.postMessage({
                type: 'CHECK_DATA',
                way: message.way,
                error: err,
                message: msg
            });
            // vscode.window.showErrorMessage(msg);
            return;
        }
        //接收的关键字为CREATE_PROJECT_COMPLETED
        else if (message.type === "CREATE_PROJECT_COMPLETED") {
            var msg = "";
            msg = await this.checkData(message, true);

            if (msg == "") {
                //创建项目
                this.createProject(message, viewPanel);

            } else {
                utils.showError(msg);
            }
            return;
        }
    };

    /**
 * 创建项目
 * @param message 用户配置数据
 */
    createProject = async (
        message: any,
        viewPanel: vscode.WebviewPanel
    ) => {
        //test
        console.log("CompileMoudle createProject");
        console.log("type : " + message.data.projectType);
        console.log("buildType : " + message.data.buildTools);
        console.log("subtype : " + message.data.projectSubType);

        //项目类型: c  c++ ...
        const type = message.data.projectType;
        // 子类型
        const subType = message.data.projectSubType;
        // 项目构建类型 makefile cmake ...
        const buildTools = message.data.buildTools;

        if (message.data.projectType && message.data.projectSubType) {
            //创建项目主目录, java maven 不在此处创建目录
            const projectDir = `${message.data.projectPath}/${message.data.projectName}`;
            await utils.createDir(projectDir);

            if (message.data.projectSubType === "default") {
                // 创建目录
                await utils.createDir(`${projectDir}/include`);
                // 创建目录
                await utils.createDir(`${projectDir}/lib`);
            }

            if (buildTools === "cmake") {
                await utils.createDir(`${projectDir}/build`);
            }

            // 拷贝其它模板文件
            await this.createFiles(message, projectDir);

            // 写入配置参数

            //将创建的项目显示到工作区
            const uri = vscode.Uri.file(projectDir);
            //页面消失操作
            viewPanel.dispose();
            // 执行打开新工程命令
            vscode.commands.executeCommand("vscode.openFolder", uri);

            utils.showInfo(localize("language.CreateProjectSuccess"));
        } else {
            utils.showError(localize("language.CreateProjectError"));
        }
    }

    /**
   * 创建项目文件夹
   * @param message 用户配置数据
   * @param projectPath 本地项目目录
   * @returns
   */
    createFiles = async (message: any, projectPath: string) => {
        try {
            //项目类型: c  c++ ...
            const type = message.data.projectType;
            // 子类型
            const subType = message.data.projectSubType;
            // 项目构建类型 makefile cmake ...
            const buildTools = message.data.buildTools;
            console.log(path.join(gContext.extensionPath, "templates", type, subType, buildTools))
            //拷贝项目所有文件到项目目录下
            fse.copySync(
                path.join(gContext.extensionPath, "templates", type, subType, buildTools),
                projectPath
            );

            if (buildTools == "makefile" || buildTools == "cmake") {

                let filename = undefined;
                if (type == "c") {
                    filename = "src/main.c"
                }
                if (filename != undefined) {
                    // 修改作者信息等
                    await modifyAuthorInfo(projectPath, message, filename);
                }
            }

            if (buildTools == "cmake") {

                let filename = undefined;
                if (type == "c") {
                    filename = "CMakeLists.txt"
                }
                if (filename != undefined) {
                    // 修改CMakeLists.txt信息等
                    await modifyCMakeListsInfo(projectPath, message, filename);
                }
            }


            if (
                (type === "c" &&
                    // subType != "blank" &&
                    message.data.CompileC != "/usr/bin/gcc") ||
                (type === "c++" &&
                    // subType != "blank" &&
                    message.data.CompileC != "/usr/bin/g++")
            ) {
                // 修改编译器路径
                await modifyCompilePath(projectPath, message);
            }
        } catch (err) {
            console.error(err);
        }
    }

    checkData = async (message: any, isCheckAll: boolean|false) => {
        let msg = "";
        if(message.data === undefined || (!isCheckAll && message.dataPages === undefined)){
            // 数据不完整 报错
            msg = localize("language.errorData");
        }
        if (isCheckAll || message.page >= message.dataPages.projectName) {
            // 判断项目名称
            if (!await utils.isProjectNameLegal(message.data.projectName)) {
                msg = localize("language.errorProjectName");
            }
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.projectPath)) {
            // 判断项目目录
            if (msg == '') {
                msg = await utils.isProjectPathLegal(message.data.projectPath, message.data.projectName);
            }
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.outputFileName)) {
            // 判断输出文件名 outputFileName
            console.log("message.data.outputFileName: " + message.data.outputFileName);
            if (!await utils.isProjectNameLegal(message.data.outputFileName)) {
                msg = localize("language.errorProjectName");
            }
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.projectSubType)) {
            // 判断 projectSubType
            if (utils.isEmpty(message.data.projectSubType)) {
                msg = localize("language.errorProjectType");
            }
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.buildTools)) {
            // todo 判断 编译工具 makefile or cmake ...
        }

        if (msg == '' && (isCheckAll || message.page >= message.dataPages.author)) {
            // todo 判断 作者
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.email)) {
            // todo 判断 email
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.version)) {
            // todo 判断 version
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.licenses)) {
            // todo 判断 licenses
        }

        if (msg == '' && (isCheckAll || message.page >= message.dataPages.compileOptmizeLevel)) {
            // todo 判断 compileOptmizeLevel
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.compileDebugLevel)) {
            // todo 判断 compileDebugLevel
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.compileWarningLevel)) {
            // todo 判断 compileWarningLevel
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.includeStaticLabiary)) {
            // todo 判断 includeStaticLabiary
        }
        if (msg == '' && (isCheckAll || message.page >= message.dataPages.includeDynamicLabiary)) {
            // todo 判断 includeDynamicLabiary
        }
        return msg;
    }
    
    /**
     * 功能：检测是否是优化参数
     * @param arg 需要检测的参数
     * @returns 是返回true,否则返回false
     */
    isOptimizationPara(arg: string): boolean {
        if (
            arg === "-O0" ||
            arg === "-O1" ||
            arg === "-O2" ||
            arg === "-O3" ||
            arg === "-Os"
        )
            return true;
        else return false;
    }

    /**
     * 功能：检测是否是调式参数
     * @param arg 需要检测的参数
     * @returns 是返回true,否则返回false
     */
    isDebuggingPara(arg: string): boolean {
        if (arg === "-g" || arg === "-g1" || arg === "-g3") return true;
        else return false;
    }

    /**
    * 功能：检测是否是警告参数参数
    * @param arg 需要检测的参数
    * @returns 是返回true,否则返回false
    */
    isWarningPara(arg: string): boolean {
        // if(arg === "-Wall")
        if (arg === "-w" || arg === "-W" || arg === "-Wall") return true;
        else return false;
    }
    /**
    * 功能：检查是否安装相关共享库
    * @param libname 关键字
    * @returns 是返回true,否则返回false
    */
    isInstalled(libname: string): boolean {
        var res = false;
        if (libname == "openssl") {
            res = utils.isInstalledopenssllib();
        } else if (libname == "openmpi") {
            res = utils.isInstalledopenmpilib();
        } else if (libname == "opencl") {
            res = utils.isInstalledopencllib();
        } else if (libname == "opencv") {
            res = utils.isInstalledopencvlib();
        }
        return res;
    }
    /**
     * 功能：接收 webview 发送的消息，并处理 
     * @param viewPanel 发送信息的webview
     * @param message 发送的信息
     * @returns 
     */
    onReceiveListenerConf = async (
        viewPanel: WebviewPanel,
        message: any,
        vsprojectidejson: any
    ) => {
        var workspacePath = vscode.workspace.rootPath;
        var tasksJsonData: vscode.WorkspaceConfiguration;
        var launchJsonData: vscode.WorkspaceConfiguration;

        var compilePara: any = {};
        var cleanPara: any;
        var runPara: any;
        var compilParaIndex = -1;
        var cleanParaIndex = -1;
        var runParaIndex = -1;
        var shareLib: string[] = new Array();
        var cprojectoption = <LangProConfig>{};
        var SourceArray: string[] = new Array();
        var libArray: string[] = new Array();
        var IncludeDirArray: string[] = new Array();
        var LibDirArray: string[] = new Array();

        console.log("onReceiveListenerConf")
        if (vscode.workspace.workspaceFolders == undefined) {
            console.log("no project");
            return;
        }
        //获取tasks.json中的task任务信息
        tasksJsonData = vscode.workspace.getConfiguration("tasks");
        //获取launch.json中launch中的configurations信息
        launchJsonData = vscode.workspace.getConfiguration("launch");
        let tasksValue = tasksJsonData.get<any[]>("tasks");
        let launchValue = launchJsonData.get<any[]>("configurations");

        //检测taskValue是否获取到数值
        if (tasksValue == undefined) {
            console.log("Error:read tasks.json");
            return;
        }
        //检测launchValue是否获取到数值
        if (launchValue == undefined) {
            console.log("Error:read launch.json");
            return;
        }

        //获取build任务信息
        for (var i = 0; i < tasksValue.length; i++) {
            if (tasksValue[i].label === "build") {
                compilePara = JSON.parse(JSON.stringify(tasksValue[i]));
                compilParaIndex = i;
            } else if (tasksValue[i].label === "clean") {
                cleanPara = JSON.parse(JSON.stringify(tasksValue[i]));
                cleanParaIndex = i;
            } else if (tasksValue[i].label === "run") {
                runPara = JSON.parse(JSON.stringify(tasksValue[i]));
                runParaIndex = i;
            }
        }


        //初始化
        cprojectoption.buildTools = "";
        cprojectoption.includespath = "";
        cprojectoption.sourcefile = "";
        cprojectoption.librarites = "";
        cprojectoption.librarypath = "";
        cprojectoption.othercompileargs = "";
        cprojectoption.runargs = "";
        cprojectoption.debugging = "None";
        cprojectoption.warnings = "None";

        // const languagedata = vsprojectidejson["language"];
        // const templatedata = vsprojectidejson["template"];

        cprojectoption.projectType = vsprojectidejson != undefined ? vsprojectidejson["language"] : "";
        cprojectoption.projectSubType = vsprojectidejson != undefined ? vsprojectidejson["template"] : "";
        cprojectoption.buildTools = vsprojectidejson != undefined ? vsprojectidejson["buildTools"] : "";
        // let projectSubType = "";
        // let projectBuildTools = "";
        // for (var i = 0; i < launchValue.length; i++) {
        //     if (launchValue[i].label === "buildTools") {
        //         cprojectoption.buildTools = JSON.parse(JSON.stringify(launchValue[i]));
        //     }
        // }


        //get runargs 
        if (launchJsonData.configurations[0].arguments) {
            for (let runarg of launchJsonData.configurations[0].arguments) {
                if (runarg.length > 0) {
                    cprojectoption.runargs = cprojectoption.runargs + runarg + " ";
                }
            }
        }
        //项目目录
        if (workspacePath)
            cprojectoption.projectPath = workspacePath;
        //编译工具
        cprojectoption.compiletool = compilePara.linux.command;
        //调试工具
        cprojectoption.debugtool = launchJsonData.configurations[0].gdbpath;
        cprojectoption.isInstalledopenmp = true;

        //检查共享库是否都已安装
        utils.getSystemPlatform();
        if (utils.isLinux()) {
            cprojectoption.isInstalledopencl = this.isInstalled("opencl");
            cprojectoption.isInstalledopenssl = this.isInstalled("openssl");
            cprojectoption.isInstalledopenmpi = this.isInstalled("openmpi");
            //参数去重
            compilePara.linux.args = utils.uniqueArray(compilePara.linux.args);

            if (cprojectoption.isInstalledopenssl) {
                //检测参数中是否存在openssl的相关参数
                cprojectoption.openssl = utils.isSub(
                    compilePara.linux.args,
                    utils.getOpensslLib()
                );
                //如果有，则删除这些参数
                if (cprojectoption.openssl) {
                    compilePara.linux.args = utils.removeSub(
                        compilePara.linux.args,
                        utils.getOpensslLib()
                    );
                }
            }

            //openmpi
            if (cprojectoption.isInstalledopenmpi) {
                //检测参数中是否存在openmpi的相关参数
                if (utils.isSub(compilePara.linux.args, utils.getAllOpenmpiArgs())) {
                    cprojectoption.openmpi = true;
                }
                if (cprojectoption.openmpi) {
                    compilePara.linux.args = utils.removeSub(
                        compilePara.linux.args,
                        utils.getAllOpenmpiArgs()
                    );
                }
            }
        } else {

            cprojectoption.isInstalledopencl = false;
            cprojectoption.isInstalledopenssl = false;
            cprojectoption.isInstalledopenmpi = false;
        }

        for (var i = 0; i < compilePara.linux.args.length; i++) {
            //获取优化级别
            if (this.isOptimizationPara(compilePara.linux.args[i])) {
                cprojectoption.optimization = compilePara.linux.args[i];
                continue;
            }
            //获取调试信息
            if (this.isDebuggingPara(compilePara.linux.args[i])) {
                cprojectoption.debugging = compilePara.linux.args[i];
                continue;
            }
            //获取警告信息
            if (this.isWarningPara(compilePara.linux.args[i])) {
                cprojectoption.warnings = compilePara.linux.args[i];
                continue;
            }
            //execname
            if (compilePara.linux.args[i] === "-o") {
                cprojectoption.execname = compilePara.linux.args[i + 1];
                i++;
                continue;
            }
            //openmp
            if (compilePara.linux.args[i] === "-fopenmp") {
                cprojectoption.openmp = true;
                continue;
            }
            //opencl
            if (compilePara.linux.args[i] === "-lOpenCL") {
                cprojectoption.opencl = true;
                continue;
            }
            //获取头文件参数
            if (utils.isStartWith(compilePara.linux.args[i], "-I")) {
                IncludeDirArray.push(compilePara.linux.args[i]);
                continue;
            }
            //获取库路径
            if (utils.isStartWith(compilePara.linux.args[i], "-L")) {
                LibDirArray.push(compilePara.linux.args[i]);
                continue;
            }
            //获取源文件
            if (utils.isEndWith(compilePara.linux.args[i], ".c")) {
                SourceArray.push(compilePara.linux.args[i]);
                continue;
            }
            //获取库参数
            if (utils.isStartWith(compilePara.linux.args[i], "-l")) {
                libArray.push(compilePara.linux.args[i]);
                continue;
            }
            //other compile para
            else {
                cprojectoption.othercompileargs =
                    cprojectoption.othercompileargs + compilePara.linux.args[i] + " ";
            }
        }

        // 拼接头文件
        for (let i = 0; i < IncludeDirArray.length; i++) {
            cprojectoption.includespath += IncludeDirArray[i].substring(2) + ";";
        }

        // 拼接链接库目录
        for (let i = 0; i < LibDirArray.length; i++) {
            cprojectoption.librarypath += LibDirArray[i].substring(2) + ";";
        }
        // 拼接链接库
        for (let i = 0; i < libArray.length; i++) {
            cprojectoption.librarites += libArray[i] + " ";
        }
        // 拼接源文件
        for (let i = 0; i < SourceArray.length; i++) {
            cprojectoption.sourcefile += SourceArray[i] + " ";
        }
        //end if
        console.log("includedir:" + IncludeDirArray);
        console.log("LibDirArray:" + LibDirArray);
        console.log("libArray:" + libArray);
        console.log("SourceArray:" + SourceArray);
        console.log(cprojectoption);


        // 翻页
        if (message.type === 'CHECK_DATA') {
            var msg = '';
            console.log("message.page: " + message.page);
            if (message.page == 1) {
                // 验证内容
            } else if (message.page == 2) {
                // 验证内容
            }
            var err = 'false';
            if (msg != '') {
                err = 'true'
            }
            // 发送消息
            viewPanel.webview.postMessage({
                type: 'CHECK_DATA',
                way: message.way,
                error: err,
                message: msg
            });
            // vscode.window.showErrorMessage(msg);
            return;
        }
        //接收的关键字为“SHOW_CONFIG”，则获取信息，向webview发送信息数据
        else if (message.type == "SHOW_CONFIG") {
            // 发送消息
            viewPanel.webview.postMessage({
                // 消息类型:
                type: "SHOW_CONFIG",
                // 项目路径
                projectPath: workspacePath,
                projectOption: cprojectoption,
                shareLib: shareLib,
                //                openclInfo: "-lOpenCL",
                openmpInfo: "-fopenmp",
                opensslInfo: cprojectoption.isInstalledopenssl ? utils.getOpensslLib() : "",
                openmpiInfo: cprojectoption.isInstalledopenmpi ? utils.getAllOpenmpiArgs() : "",
            });
            return;
        }
        //监听的关键字为"MODIFY_CONFIG"，则将信息保存到tasks.json文件中
        if (message.type == "MODIFY_CONFIG") {
            console.log("message");
            console.log(message);
            var args: string[] = [];
            if (message.data.compiletool != cprojectoption.compiletool)
                compilePara.linux.command = message.data.compiletool;

            console.log(args);
            //添加优化级别
            if (message.data.optimization.length > 0)
                args.push(message.data.optimization);
            //add warnings
            if (
                message.data.warnings.length > 0 &&
                message.data.warnings != "None"
            )
                args.push(message.data.warnings);
            //add debugging
            if (
                message.data.debugging.length > 0 &&
                message.data.debugging != "None"
            )
                args.push(message.data.debugging);
            //add othercompile
            if (message.data.othercompileargs.length > 0) {
                var temArray = message.data.othercompileargs.split(" ");
                for (let src of temArray) {
                    if (src.length > 0) {
                        args.push(src);
                    }
                }
            }
            //添加openmp参数
            if (message.data.openmp) {
                args.push("-fopenmp");
            }
            //add includepath
            if (message.data.includespath.length > 0) {
                var tempArray = message.data.includespath.split(";");
                for (var i = 0; i < tempArray.length; i++) {
                    if (tempArray[i].length > 0) args.push("-I" + tempArray[i]);
                }
            }
            //add libpath
            if (message.data.librarypath.length > 0) {
                var tempArray = message.data.librarypath.split(";");
                for (var i = 0; i < tempArray.length; i++) {
                    if (tempArray[i].length > 0) {
                        args.push("-L" + tempArray[i]);
                    }
                }
            }
            //add librarites
            if (message.data.librarites.length > 0) {
                var tempArray = message.data.librarites.split(" ");
                for (var i = 0; i < tempArray.length; i++) {
                    if (tempArray[i].length > 0) {
                        args.push(tempArray[i]);
                    }
                }
            }
            //如果勾选了openmpi，则添加openmpi参数
            if (message.data.openmpi && cprojectoption.isInstalledopenmpi) {
                args = args.concat(utils.getAllOpenmpiArgs());
            }
            //如果勾选了openssl，则添加openssl参数
            if (message.data.openssl && cprojectoption.isInstalledopenssl) {
                args = args.concat(utils.getOpensslLib());
            }
            //如果勾选了opencl，则添加opencl参数
            if (message.data.opencl && cprojectoption.isInstalledopencl) {
                args.push("-lOpenCL");
            }

            if (message.data.sourcefile.length > 0) {
                var tempArray = message.data.sourcefile.split(" ");
                for (var i = 0; i < tempArray.length; i++) {
                    if (tempArray[i].length > 0) {
                        args.push(tempArray[i]);
                    }
                }
            }

            args.push("-o");
            args.push(message.data.execname);
            args = utils.uniqueArray(args);

            compilePara.linux.args = args;
            var msg = localize("language.saveSuccess");
            //write tasks.json
            // 更新编译工具
            tasksValue![compilParaIndex].linux.command = compilePara.linux.command;
            // 更新编译参数
            tasksValue![compilParaIndex].linux.args = compilePara.linux.args;
            // 更新目标文件
            tasksValue![cleanParaIndex].linux.args[0] =
                message.data.execname;

            var runArgsTemp: string[] = new Array();
            //write launch.json
            if (message.data.runargs != cprojectoption.runargs) {
                if (message.data.runargs.length > 0) {
                    let temArray = message.data.runargs.split(" ");
                    for (let src of temArray) {
                        if (src.length > 0) {
                            // 拆分 参数
                            runArgsTemp.push(src);
                        }
                    }
                }
                launchValue![0].arguments = runArgsTemp;
                // tasksValue![runParaIndex].linux.args=runArgsTemp;
            }
            // 调试工具
            launchValue![0].gdbpath = message.data.debugtool;
            launchValue![0].program =
                "${workspaceFolder}" + "/" + message.data.execname;
            tasksValue![runParaIndex].linux.command =
                "${workspaceFolder}" + "/" + message.data.execname;

            //写回launch.json
            launchJsonData.update("configurations", launchValue);

            console.log("--------------")
            console.log(tasksValue)
            // 写回 tasks.json 文件
            tasksJsonData.update("tasks", tasksValue);
            vscode.window.showInformationMessage(msg);
            viewPanel.dispose();
        }
        else if (message.type == "CANCEL_CONFIG") {
            vscode.window.showInformationMessage(localize("language.closeSuccess"));
            viewPanel.dispose();
        }
    };

    /**
     * 注册创建默认模版命令
     * @returns 
     */
    createCProject(): vscode.Disposable {
        // 注册命令
        let disposable = vscode.commands.registerCommand(
            "project.createCProject",
            () => {
                // 打开webview窗口
                ViewManager.createWebviewPanel({
                    // html 文件名
                    path: "createCProjectTemplate",
                    // 页面标题
                    title: "Create C Blank Project",
                    splitView: false,
                    // 监听
                    receiveListener: this.onReceiveListener,
                });
            }
        );
        return disposable;
    }
    // /**
    //  * 注册创建默认模版命令
    //  * @returns 
    //  */
    // cBlankCreate(): vscode.Disposable {
    //     // 注册命令
    //     let disposable = vscode.commands.registerCommand(
    //         "project.c.blanktemplate",
    //         () => {
    //             // 打开webview窗口
    //             ViewManager.createWebviewPanel({
    //                 // html 文件名
    //                 path: "cblankTemplate",
    //                 // 页面标题
    //                 title: "Create C Blank Project",
    //                 splitView: false,
    //                 // 监听
    //                 receiveListener: this.onReceiveListener,
    //             });
    //         }
    //     );
    //     return disposable;
    // }
    // /**
    //  * 注册创建默认模版命令
    //  * @returns 
    //  */
    // cDefaultCreate(): vscode.Disposable {
    //     // 注册命令
    //     let disposable = vscode.commands.registerCommand(
    //         "project.c.defaulttemplate",
    //         () => {
    //             // 打开webview窗口
    //             ViewManager.createWebviewPanel({
    //                 // html 文件名
    //                 path: "cdefaultTemplate",
    //                 // 页面标题
    //                 title: "Create C Default Project",
    //                 splitView: false,
    //                 // 监听
    //                 receiveListener: this.onReceiveListener,
    //             });
    //         }
    //     );
    //     return disposable;
    // }
    // //注册创建静态库命令
    // cStaticLibCreate(): vscode.Disposable {
    //     // 注册命令
    //     let disposable = vscode.commands.registerCommand(
    //         "project.c.staticlibtemplate",
    //         () => {
    //             // 打开webview窗口
    //             ViewManager.createWebviewPanel({
    //                 // html 文件名
    //                 path: "cstaticlibTemplate",
    //                 // 页面标题
    //                 title: "Create C Static Lib Project",
    //                 splitView: false,
    //                 // 监听
    //                 receiveListener: this.onReceiveListener,
    //             });
    //         }
    //     );
    //     return disposable;
    // }
    // //注册创建动态库命令
    // cDynamicLibCreate(): vscode.Disposable {
    //     // 注册命令
    //     let disposable = vscode.commands.registerCommand(
    //         "project.c.dynamiclibtemplate",
    //         () => {
    //             // 打开webview窗口
    //             ViewManager.createWebviewPanel({
    //                 // html 文件名
    //                 path: "cdynamiclibTemplate",
    //                 // 页面标题
    //                 title: "Create C Dynamic Lib Project",
    //                 splitView: false,
    //                 // 监听
    //                 receiveListener: this.onReceiveListener,
    //             });
    //         }
    //     );
    //     return disposable;
    // }

    // //注册显示图形配置界面命令
    // cConf() {
    //     // 打开webview窗口
    //     ViewManager.createWebviewPanel({
    //         // html 文件名
    //         path: "configProjectPage",
    //         // 页面标题
    //         title: "Config C Project",
    //         splitView: false,
    //         // 监听
    //         receiveListener: this.onReceiveListenerConf,
    //     });
    // }
}
