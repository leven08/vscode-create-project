export interface RpmConfig {
    Name: string,
    Version: string,
    Release: string,
    Summary: string,
    BuildArch: string,
    License: string,
    Vendor: string,
    Group: string,
    Packager: string,
    URL: string,
    Source: string,
    Requires: string,
    BuildRequires: string,
    BuildRoot: string,
    Description: string,
    Patch: string,
    pre: string,
    prep: string,
    post: string,
    build: string,
    install: string,
    preun: string,
    postun:string,
    files: string,
    changelog: string,
    SourceStr:string,
    fileList:[]
}

export interface DebConfig {
    Name: string,
    Version: string,
    Maintainer: string,
    Section: string,
    Essential: string,
    Homepage: string,
    Architecture: string,
    Priority: string,
    Recommends: string,
    Source: string,
    Description: string,
    Depends: string
}

export interface LangProConfig {
    projectType: string,
    projectPath: string|undefined,
    projectSubType: string,
    buildTools: string,
    execname: string,
    compiletool: string,
    debugtool:string,
    optimization: string,
    warnings: string,
    includespath:string,
    debugging: string,
    othercompileoptions:string,
    sourcefile:string;
    othercompileargs:string,
    runargs:string,
    librarypath:string,
    librarites:string,
    otherlinkoptions:[],
    otherlinkargs:[],
    openmp:boolean,
    openmpi:boolean,
    openssl:boolean,
    opencl:boolean,
    opencv:boolean,
    isInstalledopenmp:boolean,
    isInstalledopenmpi:boolean,
    isInstalledopencl:boolean,
    isInstalledopencv:boolean,
    isInstalledopenssl:boolean
}