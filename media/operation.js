// This script will be run within the webview itself
// It cannot access the main VS Code APIs directly.
(function () {
    const vscode = acquireVsCodeApi();

    // const oldState = vscode.getState() || { colors: [] };

    /** @type {Array<{ value: string }>} */
    // let colors = oldState.colors;

    // updateColorList(colors);

    // Handle messages sent from the extension to the webview
    window.addEventListener('message', event => {
        const message = event.data; // The json data that the extension sent
        switch (message.type) {
            case 'status':
                {
                    showStatus(message);
                    break;
                }
            case 'init':
                {
                    initButtons(message.list);
                    break;
                }

        }
    });



    operationInit();

    function initButtons(list){
        // debugger;
        const part = document.querySelector('.operation-part');
        // let childs = part.children;
        
        part.innerHTML = '';
        if(list != undefined && list.length > 0){
            for(var i = 0; i < list.length; i++) { 
                let item = list[i];
                if(item.status == 0){
                    continue;
                }
                // // 添加按钮
                // part.innerHTML +=  '<button class="' + item.type + '-button">' + item.title + '</button>';
                // // 添加点击事件
                // document.querySelector("." + item.type + '-button').addEventListener(item.type, () => {
                //     operationProject(item.type);
                // });
                let btn = document.createElement("button");
                btn.innerHTML = item.title;
                let className = item.type + "-button";
                btn.setAttribute("class", className);
                var p_style = document.createAttribute("style");

                if(item.status == 1){
                    p_style.value = "cursor: pointer;";
                    btn.onclick = function () {                          //绑定点击事件
                        operationProject(item.type);
                    }
                }else{
                    p_style.value = "cursor: none;";
                    btn.setAttribute("disabled", true);
                }
                btn.setAttributeNode(p_style);
                part.appendChild(btn);
                // if(item.status != 1){
                //     document.querySelector('.' + className).disabled = true;
                // }

            }
        }

        // for(var i = childs.length - 1; i >= 0; i--) { 
        //     part.removeChild(childs[i]); 
        //   }
        // if(list != undefined && list.length > 0){
        //     for(var i = 0; i < list.length; i++) { 
        //         let item = list[i];
        //         // 添加按钮
        //         let btn = document.createElement("button");
        //         btn.
        //         part.appendChild(
        //             '<button class="' + item.type + '-button">' + item.title + '</button>'
        //         )
                
        //         // 添加点击事件
        //         document.querySelector(item.type + '.config-button').addEventListener(item.type, () => {
        //             operationProject(item.type);
        //         });

        //     }
        // }
    }

    function showStatus(status){
        switch(status.code){
            case 0:
                //ok
                console.log( "status.type: " + status.type + " is ok")
                break;
            default:
                break;
        }
    }


    /** 
     * @param {string} color 
     */
    function onColorClicked(color) {
        vscode.postMessage({ type: 'colorSelected', value: color });
    }

    /**
     * @returns string
     */
    function getNewCalicoColor() {
        const colors = ['020202', 'f1eeee', 'a85b20', 'daab70', 'efcb99'];
        return colors[Math.floor(Math.random() * colors.length)];
    }

    function operationProject(type) {
        vscode.postMessage({ type: type});
    }
    function operationInit() {
        vscode.postMessage({ type: 'init'});
    }
    
    
}());


