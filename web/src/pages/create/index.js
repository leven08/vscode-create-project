import { createApp } from 'vue';
import ElementPlus from 'element-plus';

import enLocale from 'element-plus/lib/locale/lang/en';
import zhLocale from 'element-plus/lib/locale/lang/zh-cn';
import 'element-plus/dist/index.css';
import "@/assets/index.css";
import "@/assets/tailwind.css"; //引入tailwind
// import VueI18n from 'vue-i18n';
// import Home from './views/home'
// import Project from './views/project'
import router from '../../router';

import App from './index.vue';
import {i18n} from '../../languages/index';
import {global} from '@/views/components/Global';

let locale = '';
const app = createApp(App);
console.log(i18n);
console.log("===================")
if (i18n.global.fallbackLocale === 'zh') {
    locale = zhLocale;
} else {
    locale = enLocale;
}

try {
    console.log("vscode:");
    console.log(global.vscode);
} catch (e) {
    console.log(e);
}

app.use(i18n);
app.use(ElementPlus, { locale });
app.use(router);
// app.use(vs);
app.config.globalProperties.$vscode = global.vscode;


// // 动态注册公共组件
// const requireComponent = require.context(
//     // 其组件目录的相对路径
//     '@/components',
//     // 是否查询其子目录
//     true,
//     // 匹配基础组件文件名的正则表达式
//     /index.vue$/
// );
// const jieguo = requireComponent.keys().filter((item) => true);
// jieguo.forEach((item) => {
//     const componentConfig = requireComponent(item);
//     const name = item.split("/")[1];
//     app.component(name, componentConfig.default || componentConfig);
// });
// // 注册结束

app.mount('#app');
// const routes = [
//     { path: '/', component: Home },
//     { path: '/about', component: About },
//   ]

//   // 3. 创建路由实例并传递 `routes` 配置
//   // 你可以在这里输入更多的配置，但我们在这里
//   // 暂时保持简单
//   const router = VueRouter.createRouter({
//     // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
//     history: VueRouter.createWebHashHistory(),
//     routes, // `routes: routes` 的缩写
//   })

//   // 5. 创建并挂载根实例
//   const app = Vue.createApp({})
//   // app.use(ElementPlus);
//   //确保 _use_ 路由实例使
//   //整个应用支持路由。
//   app.use(router)

//   app.mount('#app')