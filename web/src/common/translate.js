function translate(t, inData) {
    //   console.log("-------translate: ");
    //  console.log(inData);
    //  console.log(Array.isArray(inData));
    //  console.log(inData instanceof Object)
    //遍历获取的数据，用this.$t()将每项数据与翻译资源对应
    if (inData != undefined) {
        if (Array.isArray(inData)) {
            try {
                inData.forEach((e, i) => {
                    // console.log("----{{e}}")
                    // console.log(inData[i])
                    inData[i] = translate(t, e);
                    // console.log(inData[i])
                });
            } catch (e) {
                console.log(e)
            }
        } else if (inData instanceof Object) {
            try {
                Object.getOwnPropertyNames(inData).forEach((k) => {
                    // console.log("===={{inData[k]}}")
                    // console.log(inData[k])
                    inData[k] = translate(t, inData[k]);
                    // console.log(inData[k])
                });
            } catch (e) {
                console.log(e)
            }
            return inData;
        } else {
            console.log("####inData:")
            console.log(inData)
            try {
                if (inData.indexOf("language.") == 0) {
                    inData = t(inData);
                }
            } catch (e) {
                console.log(e)
            }
            //  console.log(inData)
        }
    }
    return inData;
}
export {
    translate
};