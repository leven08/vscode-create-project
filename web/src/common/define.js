/* eslint-disable @typescript-eslint/naming-convention */
const keys = {
    'KEY-HELP': "key-help", // 帮助页
    'KEY-SETTING': "key-setting", // 设置
    'KEY-CONFIG-PROJECT': "key-config-project", // 配置项目
    'KEY-PACKAGE-PROJECT': "key-package-project", // 配置项目
    'KEY-PROJECT': "key-project", // 项目入口
    'KEY-IMPORT-PROJECT': "key-import-project", // 项目导入入口
    'KEY-CREATE-PROJECT': "key-create-project", // 项目新建入口

    'KEY-C': "c", // 创建C项目
    'KEY-CPLUS': "c++", // 创建C++项目
    // 'KEY-JAVA': "key-java-project", // 创建Java项目
    // 'KEY-IMPORT-C': "key-import-c-project", // 导入C项目
    // 'KEY-IMPORT-CPLUS': "key-import-cplus-project", // 导入C++项目
    // 'KEY-IMPORT-JAVA': "key-import-java-project", // 导入Java项目
    // 'KEY-GO': "key-go-project", // 创建GO项目
    'KEY-QT': "key-qt-project", // 创建Qt项目
    'KEY-QT-WIDGETS': "key-qt-widgets-project", // 创建Qt Widgets项目
};
const messageKeys = {
    'MESSAGE': 'MESSAGE',
    'CONNECTION_ERROR': 'CONNECTION_ERROR',
    'CHOOSE_DIR': 'CHOOSE_DIR',
    'GET_HOME_DIR': 'GET_HOME_DIR',
    'GET_BUILD_PATH': 'GET_BUILD_PATH'
};
export {
    keys,
    messageKeys
};