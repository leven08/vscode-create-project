 function clone(obj) {
     var copy;
     try {
         switch (typeof obj) {
             case 'undefined':
             case 'void':
                 break;
             case 'number':
             case 'string':
             case 'boolean':
                 copy = obj;
                 break;
             case 'object':
                 if (obj == null) { copy = null; } else if (toString.apply(obj) === '[object Array]') {
                     copy = [];
                     for (var i in obj) { copy.push(clone(obj[i])); }
                 } else {
                     copy = {};
                     for (var j in obj) { copy[j] = clone(obj[j]); }
                 }
         }
     } catch (e) {
         console.log(e);
     }
     return copy;
 };

 export {
     clone,
 };