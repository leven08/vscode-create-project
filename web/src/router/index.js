import { createRouter, createWebHashHistory, createWebHistory  } from "vue-router";
// 导入组件
// import HomePage from '../views/HomePage.vue';
import CreateProjectPage from '../views/CreateProjectPage.vue';
// import AboutPage from "../views/AboutPage.vue";
 
// import Vue from 'vue';

const routes = [
  { path: "/", component: CreateProjectPage },
  // { name: "index", path: "/index", component: HomePage },
  { name: "create", path: "/create", component: CreateProjectPage },
  // { name: "help", path: "/help", component: AboutPage },
];

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHistory(),
  routes, // `routes: routes` 的缩写
});

export default router;
