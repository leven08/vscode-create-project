const path = require('path');

function resolve(dir) {
    return path.join(__dirname, dir);
}


module.exports = {
    // 基本路径
    publicPath: '/',
    // assetsDir: 'assets',
    // 输出文件目录
    outputDir: '../dist',
    indexPath: 'createProjectPage.html',
    // filenameHashing: true,
    pages: {
        // homepage: {
        //     entry: 'src/pages/index/index.js',
        //     template: 'src/pages/index/index.html',
        //     filename: 'homepage.html',
        //     chunks: ['chunk-vendors', 'chunk-common', 'homepage']
        // },
        createProjectPage: {
            entry: 'src/pages/create/index.js',
            template: 'src/pages/create/index.html',
            filename: 'createProjectPage.html',
            chunks: ['chunk-vendors', 'chunk-common', 'createProjectPage']
        }
    },
    lintOnSave: false,
    configureWebpack: {
        externals: {}
    },
    // configureWebpack: {
    //     plugins: [
    //         require('unplugin-vue-components/webpack')({ /* options */ }),
    //     ],
    // },
    chainWebpack: (config) => {
        //修改文件引入自定义路径
        config.resolve.alias
            .set('@', resolve('src'))
            .set('~assets', resolve('src/assets'));
        // .set('ide',resolve('src/ide'))
        // config.resolve.extensions
        //     .add('ts')
        //     .add('tsx');

    }
};