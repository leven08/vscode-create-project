# vscode-create-project

创建编译构建打包项目
# 构建
## web 构建
    cd ${projectpath}/web
    yarn
    yarn run build

### yarn 失败
    配置淘宝源
     yarn config set registry http://registry.npm.taobao.org/

### 运行前端调试
    yarn run dev

### 插件运行调试
    Ctrl+Shift+D调起运行调试页面执行
    或F5直接运行调试。

    运行界面显示后，点击编辑器右上角“创建项目”图标即可调起创建项目页面。
    也可以通过命令行运行：project.createProject 命令调起页面。

## 插件构建
    cd ${projectpath}/
    yarn
    yarn run compile
    